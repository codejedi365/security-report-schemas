#!/bin/bash

schema_contains_selector() {
  jq -e "$2" <"$1" 2>&1 >/dev/null
}

verify_schema_contains_selector() {
  schema_contains_selector "$1" "$2"
  assert_equals 0, "$?", "$1 did not contain selector $2"
}

verify_schema_doesnt_contain_selector() {
  schema_contains_selector "$1" "$2"
  assert_not_equals 0, "$?", "$1 erroneously contains selector $2"
}

regenerate_dist_schemas() {
  npm run --silent build-schema
  assert_equals 0, "$?", "Failed to create schema for distribution"
}
