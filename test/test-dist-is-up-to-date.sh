#!/bin/bash
# Generic library-like bash_unit test file
# Expects to be called & used by schema packages

if [ -z "${SCHEMA_FILE:=}" ]; then
  echo >&2 "Error: This test script requires the global SCHEMA_FILE to be set."
  exit 1
fi

if ! (npm run-script | grep -q "^  build-schema$"); then
  echo >&2 "Error: expects to be run from a package with a npm run-script build-schema"
  exit 1
fi

read_schema() {
  local timing_adj="$1"      # BEFORE || AFTER
  local schema_filepath="$2" # filepath

  SCHEMA_CONTENTS="$(cat "${schema_filepath}")"
  assert_equals 0, "$?", "Failed to read ${schema_filepath} schema"

  # shellcheck disable=SC2140 # (Word is of the form "A"B"C")
  export "${timing_adj}_SCHEMA"="$SCHEMA_CONTENTS"
}

verify_schema_is_up_to_date() {
  local basefile="$1"
  basefile="$(basename "$basefile")"
  local MESSAGE="
The checked-in version of the $basefile schema is different to what is generated for release.
This will cause problems for releasing, as releases are made using Git Tags.
Please refer to the README for setup, run \`npm run distribute\` on your local machine, and check-in the resulting dist/*.json files into Git.
"

  diff <(echo "${BEFORE_SCHEMA}") <(echo "${AFTER_SCHEMA}")
  assert_equals 0, "$?", "$MESSAGE"
}

test_dist_is_up_to_date() {
  read_schema "BEFORE" "$SCHEMA_FILE"

  npm run build-schema >/dev/null
  assert_equals 0, "$?", "Failed to create schema for distribution"

  read_schema "AFTER" "$SCHEMA_FILE"

  git checkout HEAD -- "$SCHEMA_FILE"

  verify_schema_is_up_to_date "$SCHEMA_FILE"
}
