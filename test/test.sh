#!/bin/bash

set -e

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

if [ -z "${npm_lifecycle_event:=}" ]; then
  SCRIPT_DIRECTORY="$(dirname "$(realpath "$0")")"
else
  # npm run-script guarantees the CWD is in the project
  SCRIPT_DIRECTORY="$(realpath "$PWD")"
fi

# Monorepo project path
pushd "$SCRIPT_DIRECTORY" >/dev/null || exit 1
PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
popd >/dev/null || exit 1

pushd "$PROJECT_DIRECTORY" >/dev/null || exit 1

log "Evaluate Schema Builds"
npx lerna run test:verify-build --no-bail

log "Evaluate Javascript Specs"
npx lerna run test:unit --no-bail

popd >/dev/null || exit 1
