FROM node:14-alpine3.11

WORKDIR /usr/local/src/security-report-schemas
ARG BASH_UNIT_SHA1SUM=f96b8dfa01f953c366a10168b97d7f901e1d6f32

RUN apk add --no-cache bash curl jq git && \
      curl -s -O https://raw.githubusercontent.com/pgrange/bash_unit/v1.7.1/install.sh && \
      echo "${BASH_UNIT_SHA1SUM}  install.sh" | sha1sum -c && \
      bash <install.sh && \
      rm install.sh && \
      mv ./bash_unit /usr/local/bin && \
      chown -R node:node .

ADD --chown=node:node . .

USER node
RUN npm install --no-fund --no-package-lock && \
      npm run distribute

USER root
CMD [ "/bin/bash" ]
