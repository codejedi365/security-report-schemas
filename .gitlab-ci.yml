image: debian:buster-slim

stages:
  - test
  - release
  - deploy

.project-install:
  stage: test
  image: node:14-alpine3.11
  before_script: &project-install-script
    - apk add bash curl jq git
    - npm install --no-fund --no-package-lock
    - npm run bootstrap -- --no-ci
  except:
    - tags

jsonlint:
  extends: .project-install
  script:
    - echo 'linting json...'
    - npm run lint
  except:
    - tags

.bash_unit_tests:
  stage: test
  image: node:14-alpine3.11
  before_script:
    - *project-install-script
    - bash -c "bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)"
    - mv bash_unit /usr/local/bin
  except:
    - tags

test:
  extends: .bash_unit_tests
  script:
    - test/test.sh

# It is important this job is separate from other tests, as other tests generate schemas for distribution, which affects this test.
verify-dist-is-up-to-date:
  extends: .bash_unit_tests
  script:
    - npm run check-dist

verify-versioning:
  stage: test
  extends: .project-install
  script:
    - npm run check-versions
  except:
    - tags

release:
  stage: release
  extends: .project-install
  script:
    - ./scripts/release.sh
  when: manual
  allow_failure: false
  only:
    - master

deploy-npm:
  stage: deploy
  image: node:14-alpine3.11
  needs:
    - job: release
  before_script:
    - *project-install-script
    - apk add gettext
  script:
    - ./scripts/deploy-npm-pkg.sh
  only:
    - master

shell check:
  image: koalaman/shellcheck-alpine:v0.7.1
  stage: test
  before_script:
    - shellcheck --version
  script:
    - shellcheck **/*.sh
  except:
    - tags

shfmt:
  image: mvdan/shfmt:v3.1.0-alpine
  stage: test
  before_script:
    - shfmt -version
  script:
    - shfmt -i 2 -ci -d .
  except:
    - tags

check dockerfile:
  image: docker:19.03.12
  stage: test
  services:
    - docker:19.03.12-dind
  script:
    - docker build -t security-report-schemas .
  except:
    - tags

include:
- template: Security/SAST.gitlab-ci.yml
