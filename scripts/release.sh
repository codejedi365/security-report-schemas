#!/usr/bin/env bash

set -e

if [[ -z "$GITLAB_API_TOKEN" ]]; then
  printf "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project.\n"
  exit 1
fi

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

log "Initializing environment"
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$WORKING_DIRECTORY/changelog-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$WORKING_DIRECTORY/release-utils.sh"

PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}
DATA_TAGGED=false

# Check & tag all package versions first
PKGS="$(npx lerna changed --ndjson --all)"
IFS=$'\n' read -r -d '' -a PKGS < <(printf '%s\0' "$PKGS") # create array from saved output
CHANGED_PKGS=()

for pkgDesc in "${PKGS[@]}"; do
  PKG_VERSION="$(retrieve_pkg_version "$pkgDesc")"
  PKG_NAME="$(retrieve_pkg_name "$pkgDesc")"
  TAG_NAME="$PKG_NAME@$PKG_VERSION"

  log "Detected package $TAG_NAME, verifying if not already released."
  if (verify_version_not_released "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$TAG_NAME"); then
    CHANGED_PKGS+=("$pkgDesc")
    log "Tagging Git SHA $CI_COMMIT_SHA with $TAG_NAME"
    tag_git_commit "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$TAG_NAME" "$CI_COMMIT_SHA"
  fi
done

if [ ${#CHANGED_PKGS} -gt 0 ]; then
  DATA_TAGGED=true
fi

# Base format version/Project version
VERSION="$(changelog_last_version)"

log "Detected Secure Report Format $VERSION, verifying not already released"
if (verify_version_not_released "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION"); then
  log "Tagging Git SHA $CI_COMMIT_SHA with $VERSION"
  tag_git_commit "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION" "$CI_COMMIT_SHA"

  CHANGELOG_DESCRIPTION=$(changelog_last_description)
  RELEASE_DATA=$(build_release_json_payload "$VERSION" "$CHANGELOG_DESCRIPTION" "$PROJECT_URL")

  log "Creating GitLab release from Git tag $VERSION"
  create_gitlab_release "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$RELEASE_DATA"

  DATA_TAGGED=true
else
  log "Release $VERSION already exists."
fi

if [ "$DATA_TAGGED" == false ]; then
  error "Nothing was found that required a release or tag."
fi
