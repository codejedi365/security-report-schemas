#!/usr/bin/env bash

set -e

error() {
  printf "\n"
  printf "%s" "$*" >&2
  printf "\n"
  exit 1
}
