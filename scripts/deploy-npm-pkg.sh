#!/usr/bin/env bash

set -e

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

# publish_schemas_to_registry generates a schemas-only package to be published
# to specified npm package registries.
# arguments: [Registry URL] [Repository Auth Token] [Version] [Project URL] [PKG Scope]
publish_schemas_to_registry() {
  local registry="${1#https*://}" # remove protocol
  local registry_token="$2"
  local npm_pkg_scope="@$3"

  npm config set "unsafe-perm" true                               # enable lifecycle scripts as root in npm@v6
  npm config set "$npm_pkg_scope:registry" "https://$registry"    # set scope to location
  npm config set "//${registry%%/}/:_authToken" "$registry_token" # set auth token to registry

  npx lerna publish from-git --registry "https://$registry" --yes

  # cleanup -- remove config
  rm "$(npm config get userconfig)"
}

log "Initializing environment"
REGISTRY_URL="https://$CI_SERVER_HOST/api/v4/projects/$CI_PROJECT_ID/packages/npm/"

log "Publishing schema packages to $REGISTRY_URL"
publish_schemas_to_registry "$REGISTRY_URL" "$CI_JOB_TOKEN" "$CI_PROJECT_ROOT_NAMESPACE"
