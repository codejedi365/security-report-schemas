#!/usr/bin/env bash

set -e

if [ -z "${npm_lifecycle_event:=}" ]; then
  SCRIPT_DIRECTORY="$(dirname "$(realpath "$0")")"
else
  # npm run-script guarantees the CWD is in the project
  SCRIPT_DIRECTORY="$(realpath "$PWD")"
fi

# Resolve monorepo project path
pushd "$SCRIPT_DIRECTORY" >/dev/null
PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
popd >/dev/null

pushd "$PROJECT_DIRECTORY" >/dev/null

# Build all schemas in parallel
if (npx lerna run build-schema --no-bail); then
  echo "Done!"
else
  echo >&2 "ERROR: 1 or more schemas failed to build properly."
  exit 1
fi

popd >/dev/null
