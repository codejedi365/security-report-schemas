// ES6 Module entrypoint
import coverageFuzzingSchema from './dist/coverage-fuzzing-report-format.json';
export const CoverageFuzzingReportSchema = coverageFuzzingSchema;
export default CoverageFuzzingReportSchema;
