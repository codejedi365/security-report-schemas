// Type definitions for coverage-fuzzing-report-schema
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Coverage Fuzzing results */
export const CoverageFuzzingReportSchema: typeof import('./dist/coverage-fuzzing-report-format.json');

export default CoverageFuzzingReportSchema;
