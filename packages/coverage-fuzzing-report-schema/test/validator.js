import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/coverage-fuzzing-report-format.json'

export const coverageFuzzingReportTester = new SchemaValidator(schemaSpec)
export default coverageFuzzingReportTester
