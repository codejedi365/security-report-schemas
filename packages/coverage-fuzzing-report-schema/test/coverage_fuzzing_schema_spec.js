import builder, {buildReport, buildVulnerability} from './builder'
import {coverageFuzzingReportTester} from './validator'
import { testCommonSchemaSpec } from "schema-merge/helpers"

describe('coverage fuzzing schema', () => {

  testCommonSchemaSpec('coverage_fuzzing', builder, coverageFuzzingReportTester)

  it('should validate location', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          crash_type: 'Index-out-of-range',
          crash_state: 'file.Method.func7\nfile.Fuzz\n\n',
          stacktrace_snippet: 'panic: runtime error: index out of range'
        }
      })]
    })

    expect(coverageFuzzingReportTester.validate(report).success).toBeTruthy()
  })
})
