#!/bin/bash
# bash_unit test suite

setup_suite() {
  PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
  source "$PROJECT_DIRECTORY/test/helper_functions.sh"
  source "$PROJECT_DIRECTORY/test/common-tests.sh"
  COVERAGE_FUZZING_SCHEMA="../dist/coverage-fuzzing-report-format.json"
  regenerate_dist_schemas
}

test_coverage_fuzz_contains_common_definitions() {
  ensure_common_definitions "$COVERAGE_FUZZING_SCHEMA" '["coverage_fuzzing"]'
}

test_coverage_fuzzing_extensions() {
  local vulns=".properties.vulnerabilities.items"
  local vuln_props="$vulns.properties"

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_address"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.stacktrace_snippet"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_state"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_type"
}
