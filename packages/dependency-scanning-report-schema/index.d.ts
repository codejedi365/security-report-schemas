// Type definitions for dependency-scanning-report-schema
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Dependency Scanning results */
export const DependencyScanningReportSchema: typeof import('./dist/dependency-scanning-report-format.json');

export default DependencyScanningReportSchema;
