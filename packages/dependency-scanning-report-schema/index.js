// ES6 Module entrypoint
import dependencyScanningSchema from './dist/dependency-scanning-report-format.json';
export const DependencyScanningReportSchema = dependencyScanningSchema;
export default DependencyScanningReportSchema;
