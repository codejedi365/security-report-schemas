import builder, {buildReport, buildVulnerability, buildDependencyFiles} from './builder'
import {dependencyScanningReportTester} from './validator'
import { testCommonSchemaSpec } from "schema-merge/helpers"

describe('dependency scanning schema', () => {

  testCommonSchemaSpec('dependency_scanning', builder, dependencyScanningReportTester)

  it('should validate location', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            package: {name: 'rack'},
            version: '2.0.4',
            iid: 987654321,
            direct: true,
            dependency_path: [{iid: 123456789}]
          }
        },
      })]
    })

    expect(dependencyScanningReportTester.validate(report).success).toBeTruthy()
  })

  it('should validate dependency files', () => {
    const report = buildReport({
      dependency_files: [buildDependencyFiles({
        path: 'src/web.api/packages.lock.json',
        package_manager: 'nuget',
        dependencies: [{
          iid: 44,
          dependency_path: [{iid: 35}],
          package: {name: 'Antlr3.Runtime'},
          version: '3.5.1'
        }, {
          iid: 69,
          dependency_path: [{iid: 35}],
          package: {name: 'Iesi.Collections'},
          version: '4.0.4'
        }]
      })]
    })

    expect(dependencyScanningReportTester.validate(report).success).toBeTruthy()
  })

})
