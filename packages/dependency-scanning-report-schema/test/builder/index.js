import {report} from './report'
import {vulnerability} from './vulnerability'
import {scan} from './scan'
import {dependency_files} from './dependency_files'

export const buildReport = report
export const buildVulnerability = vulnerability
export const buildScan = scan
export const buildDependencyFiles = dependency_files

export default {
  report,
  vulnerability,
  scan,
  dependency_files
}
