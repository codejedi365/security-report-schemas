import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/dependency-scanning-report-format.json'

export const dependencyScanningReportTester = new SchemaValidator(schemaSpec)
export default dependencyScanningReportTester
