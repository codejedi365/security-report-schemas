// Type definitions for dast-report-schema
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Dynamic Application Security Testing (DAST) results */
export const DastReportSchema: typeof import('./dist/dast-report-format.json');

export default DastReportSchema;
