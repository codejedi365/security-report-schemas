import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/dast-report-format.json'

export const dastReportTester = new SchemaValidator(schemaSpec)
export default dastReportTester
