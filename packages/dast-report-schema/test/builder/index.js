import {report} from './report'
import {vulnerability} from './vulnerability'
import {scan} from './scan'

export const buildReport = report
export const buildVulnerability = vulnerability
export const buildScan = scan

export default {
    report,
    vulnerability,
    scan
}
