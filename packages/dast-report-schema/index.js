// ES6 Module entrypoint
import dastSchema from './dist/dast-report-format.json';
export const DastReportSchema = dastSchema;
export default DastReportSchema;
