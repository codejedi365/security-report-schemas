// Type definitions for secret-detection-report-schema
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Secret Detection results */
export const SecretDetectionReportSchema: typeof import('./dist/secret-detection-report-format.json');

export default SecretDetectionReportSchema;
