import builder, {buildReport, buildVulnerability} from './builder'
import {secretDetectionReportTester} from "./validator"
import { testCommonSchemaSpec } from "schema-merge/helpers"

describe('secret detection schema', () => {

  testCommonSchemaSpec('secret_detection', builder, secretDetectionReportTester)

  it('should validate location', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          file: 'Main.java',
          class: 'Main',
          method: 'main',
          start_line: 10,
          end_line: 20,
          commit: {
            author: 'Fred FlintStone',
            date: '2020-01-01T00:00:00Z',
            sha: '7fa11597121a966f7b2c5ac5bc91b70e0c8d0558',
            message: 'Yabba Dabba Doo'
          }
        }
      })]
    })

    expect(secretDetectionReportTester.validate(report).success).toBeTruthy()
  })

  it('location commit is required', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {commit: undefined}
      })]
    })

    expect(secretDetectionReportTester.validate(report).errors).toContain('location requires property "commit"')
  })

  it('commit sha is required', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          commit: {
            author: 'Fred FlintStone',
            date: '2020-01-01T00:00:00Z',
            message: 'Yabba Dabba Doo'
          }
        }
      })]
    })

    expect(secretDetectionReportTester.validate(report).errors).toContain('commit requires property "sha"')
  })
})
