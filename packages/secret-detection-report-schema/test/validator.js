import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/secret-detection-report-format.json'

export const secretDetectionReportTester = new SchemaValidator(schemaSpec)
export default secretDetectionReportTester
