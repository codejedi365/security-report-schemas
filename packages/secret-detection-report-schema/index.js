// ES6 Module entrypoint
import secretDetectionSchema from './dist/secret-detection-report-format.json';
export const SecretDetectionReportSchema = secretDetectionSchema;
export default SecretDetectionReportSchema;
