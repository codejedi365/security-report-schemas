# GitLab Secret-Detection Security Reports

This package provides the schema definition for the security reports emitted by GitLab security scanners. The primary documentation for this security report type can be found at the [Secret Detection Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/) homepage.

The schema is defined using [JSON Schema](https://json-schema.org/). Any security scanner that integrates into GitLab's Secret-Detection widgets must produce a JSON report that adheres to this schema.

More information about the how and why of the schemas can be found by watching the [Security Report Format Brown Bag Session](https://youtu.be/DqKsdNLXxes).

## Schema Versioning

This package follows the [SchemaVer](https://github.com/snowplow/iglu/wiki/SchemaVer) standard `MODEL.REVISION.ADDITION` to version JSON schemas. The package version mirrors the current standard version for the schema.

### Handling Updates

Review the [`CHANGELOG.md`](./CHANGELOG.md) to read through the latest changes in the schema definition.

### Additional Properties

Secure schemas allow for additional properties to be present in JSON files. This means that the schemas are only concerned with fields in a Secure Report that are defined by the schema. The presence of any additional fields will not cause validation to fail.

This is useful for products that produce Secure Reports:

- Experimental fields can be added to a Secure Report, without affecting how the report is used.
- It allows the product to be ahead of the Secure Report Format, when the product team is confident new fields will be merged into the schemas.

Any additional properties added to a Secure Report are considered experimental and may not be supported. For this reason, adding optional fields to the Secure Report Format is considered an `ADDITION`, not a `REVISION` change.

## Bugs & Contributing
If you find a bug, please report it to our issues queue at [gitlab.org/.../security-report-schemas/issues](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/issues)

If you want to help and extend the list of supported scanners, read the contribution guidelines [gitlab.org/.../security-report-schemas/CONTRIBUTING.md](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/CONTRIBUTING.md)
