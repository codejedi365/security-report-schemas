#!/usr/bin/env bash
# [Optional] arg[1]: directory (default: CWD)

set -e

if ! command -v realpath >/dev/null; then
  realpath() { # mac os compatibility
    local OURPWD="$PWD"
    cd "$(dirname "$1")"
    local LINK=$(readlink "$(basename "$1")")
    while [ "$LINK" ]; do
      cd "$(dirname "$LINK")"
      local LINK=$(readlink "$(basename "$1")")
    done
    local REALPATH="$PWD/$(basename "$1")"
    cd "$OURPWD"
    echo "$REALPATH"
  }
fi

SCRIPT_DIRECTORY="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

os=""
case $(uname -a) in
  Linux*) os="linux" ;;
  Darwin*) os="mac" ;;
  *)
    echo "unsupported os"
    exit 1
    ;;
esac

dir="$([ -d "$1" ] && echo "${1%/}" || echo "$PWD")"

# shellcheck disable=SC2044
for i in $(find $dir -name '*.json' -type f); do
  echo -n "$i: "
  "${SCRIPT_DIRECTORY}/jsonlint-${os}" <"$i"
done
