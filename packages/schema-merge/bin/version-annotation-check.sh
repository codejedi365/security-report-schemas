#!/bin/bash

set -e

if [ -z "${npm_lifecycle_event:=}" ]; then
  echo >&2 "Error: expects to be run through an npm run-script env. Please try again."
  exit 1
fi

PROJECT_DIR="$(realpath "$(git rev-parse --show-toplevel)")"
SCRIPT_DIR="$(realpath "$(dirname "$(realpath "$0")")")"
PKG_DIR="$(realpath "$PWD")"

SECURE_REPORT_BASE_FORMAT=$(realpath "$SCRIPT_DIR/../src/security-report-format.json")
SECURE_REPORT_FORMAT_DIST="$(realpath "$PKG_DIR/dist"/*-report-format.json)"
PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/error-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/changelog-utils.sh"

# report_schema_version returns the version of a JSON report schema.
report_schema_version() {
  local schema_file="$1"

  if ! [[ -f "$schema_file" ]]; then
    error "Unable to find $schema_file."
  fi

  local version
  version=$(jq -r -e ".self.version" "$schema_file")

  if [[ -z "$version" ]]; then
    error "Aborting, unable to determine the latest self.version in $schema_file."
  fi

  echo "v$version"
}

# compare_schema_content_with_dist compares the content of a JSON schema
# with the content of its distribution, and fails if they are different.
compare_schema_content_with_dist() {
  local version="$1"
  local report_type="${2%-report-format.json}"
  report_type="$(basename "$report_type")"
  local temp_file="$TMPDIR/$report_type.schema.json"

  curl --silent --fail --output "$temp_file" "$PROJECT_URL/-/raw/$version/dist/$report_type-report-format.json"

  # temporarily disable error handling, so we can display a better error message
  set +e
  diff -u "$temp_file" "dist/$report_type-report-format.json"
  local comparison_result="$?"
  set -e

  if [[ "$comparison_result" != "0" ]]; then
    echo
    error "The released version of the $report_type schema differs from what is in the dist directory, yet" \
      "the versions are the same." \
      "Did you change the schema, and forget to increment the version?"
  fi
}

VERSION_ALREADY_RELEASED=false
CHANGELOG_VERSION="$(PROJECT_DIR="$PKG_DIR" changelog_last_version)"
BASE_REPORT_VERSION=$(report_schema_version "$SECURE_REPORT_BASE_FORMAT")
PKG_VERSION="v$(jq -r -e ".version" "$PKG_DIR/package.json")"
PKG_NAME="$(jq -r -e ".name" "$PKG_DIR/package.json")"
DIST_VERSION="$(report_schema_version "$SECURE_REPORT_FORMAT_DIST")"

# If curl returns a 404, we know the version has not been released.
if curl -X HEAD --silent --fail --output /dev/null "$PROJECT_URL/-/releases/${PKG_NAME##*/}@${CHANGELOG_VERSION#v}"; then
  VERSION_ALREADY_RELEASED=true
fi

echo "$PKG_NAME:"
echo "Version found in base secure-report-format: $BASE_REPORT_VERSION"
echo "Version found in package.json:              $PKG_VERSION"
echo "Version found in dist/:                     $DIST_VERSION"
echo "Version found in CHANGELOG:                 $CHANGELOG_VERSION"
echo "Changelog version already released:         $VERSION_ALREADY_RELEASED"
echo

if ! (node -e "const satisfies=require('semver/functions/satisfies'); process.exit(satisfies(\"$PKG_VERSION\",\">=$BASE_REPORT_VERSION\")?0:1);"); then
  error "Aborting, the source JSON version is out-of-date compared to the base report format." \
    "Please make sure the pkg versions are updated when the base format is updated."
fi

if [[ "$PKG_VERSION" != "$DIST_VERSION" ]]; then
  error "Aborting, the version in the package.json and the version in the distributed JSON have diverged." \
    "Please make sure to rebuild the distribution schema so it is up-to-date."
fi

if [[ "$PKG_VERSION" != "$CHANGELOG_VERSION" ]]; then
  error "Aborting, the version in the CHANGELOG and the schema package.json version have diverged." \
    "Do you need to add a CHANGELOG entry, or did you forget to bump the schema package version?"
fi

if [[ "$VERSION_ALREADY_RELEASED" == "true" ]]; then
  compare_schema_content_with_dist "$PKG_VERSION" "$SECURE_REPORT_FORMAT_DIST"
fi

echo "Nice work! All checks have passed."
