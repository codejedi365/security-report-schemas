#!/usr/bin/env node

const fs = require('fs');
const merger = require('json-schema-merge-allof');
const refparser = require('json-schema-ref-parser');
const vulDetails = require('./vulnerability-details-format.json');

if (process.argv.length != 3) {
  console.log("Schema file is required");
  process.exit(1);
}

const schemaFile = process.argv[2];

try {
  const stat = fs.statSync(schemaFile);
} catch (e) {
  console.log("Error reading schema file:", e.code);
  process.exit(e.errno);
}

(async () => {
  const f = schemaFile;
  const p = new refparser();
  const o = await p.dereference(f, { resolve: { ignoredExternalRefs: ["vulnerability-details-format.json"] } });
  const r = merger(o, {
    resolvers: {
      self: merger.options.resolvers.default // Takes the first found entry
    }
  });
  r.definitions = vulDetails.definitions;
  r.properties.vulnerabilities.items.properties.details = { "$ref": "#/definitions/named_list/properties/items" };
  console.log(JSON.stringify(r));
})();
