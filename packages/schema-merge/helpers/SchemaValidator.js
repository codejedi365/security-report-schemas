import { validate } from "jsonschema"

export class SchemaValidator {

    constructor(schemaSpec) {
        this.schemaSpec = schemaSpec
    }

    validate(report) {
        const result = validate(report, this.schemaSpec)
        return {
            success: result.errors.length === 0,
            errors: result.errors.join('; ')
        }
    }
}

export default SchemaValidator
