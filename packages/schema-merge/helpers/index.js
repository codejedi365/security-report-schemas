import { SchemaValidator as SV } from "./SchemaValidator";
import { merge as m } from "./merge";
import { testCommonSchemaSpec as tCSS } from "./common_schema_spec";

export const SchemaValidator = SV;
export const merge = m;
export const testCommonSchemaSpec = tCSS;

export default {
    SchemaValidator,
    merge,
    testCommonSchemaSpec
}
