import * as jsonDraft07Specification from './json-schema-draft-07.json'
import { SchemaValidator } from "./SchemaValidator"
import {withVulnerabilityDetails} from './builder/vulnerability_details'

/**
 * Applys common schema test cases to a given schema from defined report builder functions
 * @param {string} name that describes schema such as `dast`
 * @param {{ report: (overrides?: {}) => object, vulnerability: (overrides?: {}) => object, scan: (overrides?: {}) => object }} builder 
 * @param {SchemaValidator} schema A SchemaValidator object with a validate() to find errors from a given report from a schema
 */
export function testCommonSchemaSpec(name, builder, schema) {
  const jsonSpecSchema = new SchemaValidator(jsonDraft07Specification)

  describe(`common[${name}] schema`, () => {
    it('should not validate when there are no identifiers', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
          identifiers: []
        })]
      })
  
      expect(schema.validate(report).errors).toContain('identifiers does not meet minimum length of 1')
    })
  
    it('should not validate with empty identifier[].type', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
          identifiers: [{
            type: "",
            name: "name",
            value: "value"
          }]
        })]
      })
  
      expect(schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].type does not meet minimum length of 1')
    })
  
    it('should not validate with empty identifier[].name', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "",
            value: "value"
          }]
        })]
      })
  
      expect(schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].name does not meet minimum length of 1')
    })
  
    it('should not validate with empty identifier[].value', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "name",
            value: ""
          }]
        })]
      })
  
      expect(schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].value does not meet minimum length of 1')
    })
  
    it('must have a version', () => {
      const report = builder.report({version: undefined})
      expect(schema.validate(report).errors).toContain('requires property "version"')
    })
  
    it('can have zero vulnerabilities', () => {
      const report = builder.report({vulnerabilities: []})
      expect(schema.validate(report).success).toBeTruthy()
    })
  
    it('validates with additional details about vulnerabilities', () => {
      const report = withVulnerabilityDetails(builder.report());
      expect(schema.validate(report).success).toBeTruthy();
    });
  
    it('validates source tracking type', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
          tracking: {
            type: "source",
            items: [{
              file: "app/controllers/groups_controller.rb",
              start_line: 6,
              end_line: 6,
              signatures: [{
                "algorithm": "scope_offset",
                "value": "app/controllers/groups_controller.rb|GroupsController[0]|new_group[0]:4"
              }]
            }]
          }
        })]
      })
  
      expect(schema.validate(report).success).toBeTruthy();
    })
    
    it('validates flags type', () => {
      const report = builder.report({
        vulnerabilities: [builder.vulnerability({
            flags: [{
                type: "flagged-as-likely-false-positive",
                origin: "post analyzer X",
                description: "static string to sink"
            },{
                type: "flagged-as-likely-false-positive",
                origin: "post analyzer Y",
                description: "integer to sink"
            }]
        })]
      })

      expect(schema.validate(report).success).toBeTruthy();
    })

    it('complies to the JSON specification', () => {
      const report = builder.report()
      expect(jsonSpecSchema.validate(report).success).toBeTruthy()
    })
  
    it('validates scan analyzer', () => {
      const report = builder.report({
        scan: builder.scan({
          analyzer: {
            id: 'gitlab-dast',
            name: 'GitLab DAST',
            url: 'https://gitlab.com/dast',
            version: '1.6.20',
            vendor: {
              name: 'GitLab'
            }
          }
        })
      })
  
      expect(schema.validate(report).success).toBeTruthy();
    })
  })
}

export default testCommonSchemaSpec
