#!/bin/bash
# bash_unit test suite

PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
SCHEMA_FILE="$(realpath ../dist/sast-report-format.json)"

source "$PROJECT_DIRECTORY/test/test-dist-is-up-to-date.sh"
