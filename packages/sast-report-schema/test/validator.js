import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/sast-report-format.json'

export const sastReportTester = new SchemaValidator(schemaSpec)
export default sastReportTester
