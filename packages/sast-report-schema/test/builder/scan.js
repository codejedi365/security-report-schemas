import { merge } from 'schema-merge/helpers'

const defaults = {
  scanner: {
    id: 'kubesec',
    name: 'Kubesec',
    url: 'https://github.com/controlplaneio/kubesec',
    vendor: {
      name: 'GitLab'
    },
    version: '2.6.0'
  },
  type: 'sast',
  start_time: '2020-09-18T20:06:51',
  end_time: '2020-09-18T20:07:01',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
