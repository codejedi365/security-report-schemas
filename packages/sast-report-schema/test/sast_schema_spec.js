import builder, {buildReport, buildVulnerability} from './builder'
import {sastReportTester} from "./validator"
import { testCommonSchemaSpec } from "schema-merge/helpers"

describe('sast schema', () => {

  testCommonSchemaSpec('SAST', builder, sastReportTester)

  it('should validate location', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          file: 'Main.java',
          class: 'Main',
          method: 'main',
          start_line: 10,
          end_line: 20
        }
      })]
    })

    expect(sastReportTester.validate(report).success).toBeTruthy()
  })

})
