#!/bin/bash
# bash_unit test suite

setup_suite() {
  PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
  source "$PROJECT_DIRECTORY/test/helper_functions.sh"
  source "$PROJECT_DIRECTORY/test/common-tests.sh"
  SAST_SCHEMA="../dist/sast-report-format.json"
  regenerate_dist_schemas
}

test_sast_contains_common_definitions() {
  ensure_common_definitions "$SAST_SCHEMA" '["sast"]'
}

test_sast_extensions() {
  verify_schema_contains_selector "$SAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.raw_source_code_extract"
}
