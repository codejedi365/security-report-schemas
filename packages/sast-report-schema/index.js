// ES6 Module entrypoint
import sastSchema from './dist/sast-report-format.json';
export const SastReportSchema = sastSchema;
export default SastReportSchema;
