// Type definitions for security-report-schemas
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Static Application Security Testing (SAST) results */
export const SastReportSchema: typeof import('./dist/sast-report-format.json');

export default SastReportSchema;
