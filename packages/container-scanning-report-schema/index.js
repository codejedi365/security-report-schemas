// ES6 Module entrypoint
import containerScanningSchema from './dist/container-scanning-report-format.json';
export const ContainerScanningReportSchema = containerScanningSchema;
export default ContainerScanningReportSchema;
