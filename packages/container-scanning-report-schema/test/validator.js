import { SchemaValidator } from 'schema-merge/helpers'
import schemaSpec from '../dist/container-scanning-report-format.json'

export const containerScanningReportTester = new SchemaValidator(schemaSpec)
export default containerScanningReportTester
