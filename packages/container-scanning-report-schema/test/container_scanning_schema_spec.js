import builder, {buildReport, buildVulnerability} from './builder'
import {containerScanningReportTester} from './validator'
import { testCommonSchemaSpec } from "schema-merge/helpers"

describe('container scanning schema', () => {

  testCommonSchemaSpec('container_scanning', builder, containerScanningReportTester)

  it('should validate location', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(containerScanningReportTester.validate(report).success).toBeTruthy()
  })

  it('location dependency is required', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(containerScanningReportTester.validate(report).errors).toContain('location requires property "dependency"')
  })

  it('location os is required', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(containerScanningReportTester.validate(report).errors).toContain('location requires property "operating_system"')
  })

  it('location image is required', () => {
    const report = buildReport({
      vulnerabilities: [buildVulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9'
        }
      })]
    })

    expect(containerScanningReportTester.validate(report).errors).toContain('location requires property "image"')
  })

})
