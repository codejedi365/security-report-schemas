#!/bin/bash
# bash_unit test suite

setup_suite() {
  PROJECT_DIRECTORY="$(realpath "$(git rev-parse --show-toplevel)")"
  source "$PROJECT_DIRECTORY/test/helper_functions.sh"
  source "$PROJECT_DIRECTORY/test/common-tests.sh"
  CS_SCHEMA="../dist/container-scanning-report-format.json"
  regenerate_dist_schemas
}

test_container_scanning_contains_common_definitions() {
  ensure_common_definitions "$CS_SCHEMA" '["container_scanning"]'
}

test_container_scanning_extensions() {
  verify_schema_contains_selector "$CS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.image"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.operating_system"
  verify_schema_contains_selector "$CS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.package.properties.name"
}
