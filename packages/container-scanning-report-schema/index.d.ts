// Type definitions for container-scanning-report-schema
// Definitions by: GitLab Org <https://gitlab.com/gitlab-org/>


/** Report schema used for Container Scanning results */
export const ContainerScanningReportSchema: typeof import('./dist/container-scanning-report-format.json');

export default ContainerScanningReportSchema;
